<?php

namespace Database\Seeders;

use App\Models\Siswa;
use App\Models\User;
use Illuminate\Database\Seeder;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userSiswa = new User();
        $userSiswa->name = "Siswa A";
        $userSiswa->email = "siswa@gmail.com";
        $userSiswa->password = bcrypt("rahasia");
        $userSiswa->save();

        $siswa = new Siswa();
        $siswa->nis = "123456";
        $siswa->kelas = "RPL";
        $siswa->user_id = $userSiswa->id;
        $siswa->kelas_id = 1;
        $siswa->save();
    }
}
