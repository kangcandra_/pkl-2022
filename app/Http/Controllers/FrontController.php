<?php

namespace App\Http\Controllers;

use App\Models\User;

class FrontController extends Controller
{

    public function index()
    {
        $data = User::latest()->get();
        return view('welcome', compact('data'));
    }

    public function about()
    {
        return view('about');
    }
}
