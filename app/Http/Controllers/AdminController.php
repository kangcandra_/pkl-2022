<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\User;
use DB;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function getData()
    {
        $totalUser = User::count();
        // dd($totalUser);
        return view('admin.index', compact('totalUser'));
    }

    public function rekap()
    {
        $kelas = Kelas::all();
        // dd($totalUser);
        return view('admin.rekap.index', compact('kelas'));
    }

    public function rekapSiswa(Request $request)
    {
        $search = $request->kelas;
        $kelas = Kelas::find($search);
        // dd($totalUser);
        $siswa = DB::table('siswas')->join('users', 'users.id', '=', 'siswas.user_id')
            ->join('kelas', 'kelas.id', '=', 'siswas.kelas_id')
            ->select('users.name', 'siswas.nis', 'kelas.nama_kelas')
            ->where('siswas.kelas_id', '=', $search)
            ->get();
        // dd($siswa);
        return view('admin.rekap.show', compact('kelas', 'siswa'));
    }
}
