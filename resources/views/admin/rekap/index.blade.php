@extends('layouts.admin')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- [ breadcrumb ] start -->

            <!-- [ breadcrumb ] end -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- [ Main Content ] start -->
                    <div class="row">
                        <div class="col-xl-12 col-md-12 m-b-30">
                            <div class="tab-content" id="myTabContent">
                                <form action="{{route('rekap')}}" method="post">
                                    @csrf
                                    <label for="">Pilih Kelas</label>
                                    <select name="kelas" id="" class="form-control">
                                        @foreach($kelas as $kls)
                                        <option value="{{$kls->id}}">{{$kls->nama_kelas}}</option>
                                        @endforeach
                                    </select>
                                    <br>
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit">Cari Kelas</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- [ Main Content ] end -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
