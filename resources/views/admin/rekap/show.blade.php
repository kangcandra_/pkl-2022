@extends('layouts.admin')

@section('content')
<div class="pcoded-wrapper">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- [ breadcrumb ] start -->

            <!-- [ breadcrumb ] end -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- [ Main Content ] start -->
                    <div class="row">
                        <div class="col-xl-12 col-md-6">
                            <div class="card Recent-Users">
                                <div class="card-header">
                                    <h5>Data Siswa Kelas - {{$kelas->nama_kelas}}</h5>
                                </div>
                                <div class="card-block px-0 py-3">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <tbody>
                                                @foreach($siswa as $sw)
                                                <tr class="unread">
                                                    <td><img class="rounded-circle" style="width:40px;"
                                                            src="{{ asset('assets/images/user/avatar-1.jpg') }}"
                                                            alt="activity-user"></td>
                                                    <td>
                                                        <h6 class="mb-1">{{$sw->name}}</h6>
                                                        <p class="m-0">{{$sw->nis}}</p>
                                                    </td>
                                                    <td>
                                                        <h6 class="text-muted"><i
                                                                class="fas fa-circle text-c-green f-10 m-r-15"></i>11
                                                            MAY 12:56</h6>
                                                    </td>
                                                    <td><a href="#!"
                                                            class="label theme-bg2 text-white f-12">Reject</a><a
                                                            href="#!" class="label theme-bg text-white f-12">Approve</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- [ Main Content ] end -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
